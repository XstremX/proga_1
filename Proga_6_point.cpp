#include "point.h"

point::point(double x, double y)
{
    input(x, y);
}

point::point(const point& p)
{
    input(p.x, p.y);
}

point::~point()
{
    
}

point point::operator-() {
    point x = *this;
    x.invert();
    return x;
}


point point::operator*(int num) {
    point x = *this;
    x.multi(num);
    return x;
}

double point::operator-(const point& p) {
    return dist(p);
}


void point::invert()
{
    x = -x;
    y = -y;
    
}

void point::multi(double a)
{
    x *= a; y *= a;
}



void point::GetPolarCoord(double &r, double &a)
{
    r = sqrt(pow((double)x, 2.0) + pow((double)y, 2.0));
    a = acos(x / r);
}

double point::dist(const point& p)
{
    return sqrt(pow((double)(x - p.x), 2.0) + pow((double)(y - p.y), 2.0));
}

void point::input(double x, double y)
{
    this->x = x;
    this->y = y;
}

void point::output()
{
    std::cout << "Координаты точки: (" << x << "; " << y << ")" << std::endl;
}
