//vivesti na ikran process umnojeniya dvuh natural chisel
#include <iostream>
#include <iomanip>
#include <cstdio>

using namespace std;

int main(void)
{

	int a, b, c = 1, d, g, k=2, s, w=0;

	cout << "Vvedite pervoe chislo: " << endl;
	cin >> a;
	cout << "Vvedite vtotoe chislo: " << endl;
	cin >> b;

	g = a*b;
	s = b;
	while (g>0)
	{
		g /= 10;
		k++;
	}
	while (s>0)
	{
		s /= 10;
		w++;
	}

	cout << endl;
	cout << std::setw(k) << right << a << endl;
	cout << std::setw(k-w) << right << "x " << b << endl;
	for (int i = 0; i <= k; i++) cout << "-";
	cout << endl;
	cout << std::setw(k) << right << (a*(b % 10)) << endl;

	d = b;
	b = b / 10;

	while (b>0)
	{
		cout << std::setw(k-c) << right << (a*(b % 10));

		b = b / 10;
		c++;
		cout << endl;
	}

	for (int i = 0; i <= k; i++) cout << "-";
	cout << endl;
	cout << std::setw(k) << right << a*d << endl;

	system("PAUSE");
	return 0;
}