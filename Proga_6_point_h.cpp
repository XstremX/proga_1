#include "math.h"
#include <iostream>

class point {
public:
    point(double, double);
    point(const point&);
    ~point();
    
    void invert();
    void multi(double);
    void GetPolarCoord(double &, double &);
    double dist(const point& );
    
    void input(double, double);
    void output();
    point operator-();
    double operator-(const point&);
    point operator*(int);
    
    
    
private:
    double x, y;
};
