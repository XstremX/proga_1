//polzovatel zadaet summu deneg menishe 100. 
//opredelit kak vidat summu monetami po 5,2 and 1 
//izrashodovav naimen`shee chislo monet
#include <cstdio>
#include <iostream>

using namespace std;

int main(void)
{

	int a, b;

snova:
	cout << "Vvedite chislo men`she 100: " << endl;
	cin >> a;
	if (a > 100)
	{
		cout << "Chislo ne pothodit!" << endl;
		goto snova;
	}

	b = a / 5;

	cout << b << " monet nominalom 5 " << endl;

	a = a % 5;
	b = a / 2;

	cout << b << " monet nominalom 2 " << endl;

	a = a % 2;

	cout << a << " monet nominalom 1 " << endl;

	system("PAUSE");
	return 0;
}